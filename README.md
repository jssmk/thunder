# Thunder

Graphics and led effect app. Inspired by the CCCamp23 weather. Aims to extend the perceived graphics impression outside of the display screen by interacting the LEDs like they were in the same coordinate space.
