# Coordinates found in flow3r hardware git repository
COOR = [
    ( -0.000, -30.084, 180.0),
    ( -7.636, -35.883, -142.4),
    (-16.025, -41.521, -149.8),
    (-25.067, -46.038, -157.1),
    (-35.693, -49.128, 144.0),
    (-36.038, -38.066,  85.1),
    (-34.537, -28.071,  77.8),
    (-31.767, -18.351,  70.4),
    (-28.611,  -9.296, 108.0),
    (-36.486,  -3.826, 145.6),
    (-44.441,   2.410, 138.2),
    (-51.530,   9.614, 130.9),
    (-57.753,  18.765,  72.0),
    (-47.340,  22.511,  13.1),
    (-37.370,  24.172,   5.8),
    (-27.269,  24.541,  -1.6),
    (-17.683,  24.338,  36.0),
    (-14.913,  33.518,  73.6),
    (-11.441,  43.010,  66.2),
    ( -6.781,  51.979,  58.9),
    (  0.000,  60.725,  -0.0),
    (  6.781,  51.979, -58.9),
    ( 11.441,  43.010, -66.2),
    ( 14.913,  33.518, -73.6),
    ( 17.683,  24.338, -36.0),
    ( 27.269,  24.541,   1.6),
    ( 37.370,  24.172,  -5.8),
    ( 47.340,  22.511, -13.1),
    ( 57.753,  18.765, -72.0),
    ( 51.530,   9.614, -130.9),
    ( 44.441,   2.410, -138.2),
    ( 36.486,  -3.826, -145.6),
    ( 28.611,  -9.296, -108.0),
    ( 31.767, -18.351, -70.4),
    ( 34.537, -28.071, -77.8),
    ( 36.038, -38.066, -85.1),
    ( 35.693, -49.128, -144.0),
    ( 25.067, -46.038, 157.1),
    ( 16.025, -41.521, 149.8),
    (  7.636, -35.883, 142.4),
]

PALETTE = [
  [1.0, 1.0, 1.0],
  [0.5, 1.0, 1.0], 
  [1.0, 0.5, 1.0],
  [1.0, 0.8, 0.4],
  [0.9, 1.0, 0.8]]
  
from st3m.application import Application, ApplicationContext
from ctx import Context
import st3m.run

from random import randint
import leds
from time import sleep_ms



class Thunder(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        global PALETTE

        self.vecs_r = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.vecs_g = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.vecs_b = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        self.col = PALETTE[0]
        self.prev = 0 # index of start LED
        self.new = 20 # index of target LED
        self.last_x = [0.0, 0.0]
        self.last_y = [0.0, 0.0]
        self.scale = 6.0 # scales the graphics, 6.0 is good
        self.interpolation_steps = 50
        self.inter = 0
        
        super().__init__(app_ctx)
    
    def interpolate(self, ctx, prev, new, w) -> None:
        global COOR

        # bit different effect with power of two interpolation
        #w = w * w
        
        # color change effect
        new_col = [1.0 - (1.0 - self.col[0])*w ,
                   1.0 - (1.0 - self.col[1])*w ,
                   1.0 - (1.0 - self.col[2])*w ]
        
        
        # next step in the interpolation
        new_x = [0.0, 0.0]
        new_y = [0.0, 0.0]
        
        new_x[0] = self.scale * (COOR[self.new][1]*w + COOR[prev][1]*(1-w))
        new_y[0] = self.scale * (COOR[self.new][0]*w + COOR[prev][0]*(1-w))
        new_x[1] = self.scale * (COOR[self.new][1]*w + COOR[prev][1]*(1-w))
        new_y[1] = self.scale * (COOR[self.new][0]*w + COOR[prev][0]*(1-w))

        # add some random in new location

        new_x[0] = new_x[0] + randint(-15,15)
        new_y[0] = new_y[0] + randint(-15,15)

        ctx.move_to(self.last_x[0], self.last_y[0])
        ctx.rgb(*new_col).line_to(new_x[0], new_y[0]).stroke()

        ctx.move_to(self.last_x[0], self.last_y[0])
        ctx.rgb(*new_col).line_to(new_x[0] + randint(-20,20), new_y[0] + randint(-20,20)).stroke()

        # creates more lines in different places

        if (self.inter % 20) == 0:
            # new branch from here
            new_x[1] = new_x[0]
            new_y[1] = new_y[0]
            self.last_x[1] = new_x[0]
            self.last_y[1] = new_y[0]
        else:
            new_x[1] = self.last_x[1] + randint(-20,20)
            new_y[1] = self.last_y[1] + randint(-20,20)
            #new_x[1] = ((self.last_x[1] + randint(-40,40)) + self.last_x[0]) / 2
            #new_y[1] = ((self.last_y[1] + randint(-40,40)) + self.last_y[0]) / 2
        
        # these are the branches that live for a while
        ctx.move_to(self.last_x[1], self.last_y[1])
        ctx.rgb(*new_col).line_to(new_x[1], new_y[1]).stroke()

        # save this location state for next interpolation step

        self.last_x[0] = new_x[0]
        self.last_y[0] = new_y[0]
        self.last_x[1] = new_x[1]
        self.last_y[1] = new_y[1]
    
    def draw(self, ctx: Context) -> None:
        global PALETTE

        #ctx.save()
        #ctx.move_to(0,0)

        # fade to black

        ctx.rgba(0, 0, 0, 0.1).rectangle(-120, -120, 240, 240).fill()
        

        # select one of the palette colors

        self.col = PALETTE[randint(0,4)]

        # next interpolation step

        self.inter = (self.inter + 1) % self.interpolation_steps

        # align with the led coordinates so that it makes more sense
        ctx.rotate(4.712389)
        
        # 
        if self.inter == 0:
            self.prev = self.new
            self.new = (self.prev + 20 + randint(-3, 3)) % 40
            # unrolling :P
            self.vecs_r[self.prev] = self.col[0] * 255
            self.vecs_r[(self.prev - 1)%40] = self.col[0] * 128
            self.vecs_r[(self.prev + 1)%40] = self.col[0] * 128
            self.vecs_g[self.prev] = self.col[1] * 255
            self.vecs_g[(self.prev - 1)%40] = self.col[1] * 128
            self.vecs_g[(self.prev + 1)%40] = self.col[1] * 128
            self.vecs_b[self.prev] = self.col[2] * 255
            self.vecs_b[(self.prev - 1)%40] = self.col[2] * 128
            self.vecs_b[(self.prev + 1)%40] = self.col[2] * 128

        # the LED effect
        
        for j in range(40):
            j_led = (j + 20) % 40 # index offset by 20 to align with the screen
            leds.set_rgb(j_led, self.vecs_r[j], self.vecs_g[j], self.vecs_b[j])
            # Dim effect
            self.vecs_r[j] = max(self.vecs_r[j] - 15, 0)
            self.vecs_g[j] = max(self.vecs_g[j] - 15, 0)
            self.vecs_b[j] = max(self.vecs_b[j] - 15, 0)
        
        
        leds.update()
        
        # The graphics
        self.interpolate(ctx, self.prev, self.new, self.inter/self.interpolation_steps)
        
        ctx.restore()
        sleep_ms(40) # 40 works good
    
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing


if __name__ == '__main__':
#    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Thunder(ApplicationContext()))


